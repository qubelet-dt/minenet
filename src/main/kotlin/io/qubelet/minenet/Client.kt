package io.qubelet.minenet

import java.net.Socket
import java.net.SocketException
import java.util.concurrent.ConcurrentLinkedQueue
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.concurrent.thread
import kotlin.experimental.and

open class Client(private val client: Socket) {
    private val received = ConcurrentLinkedQueue<Byte>()
    private var connected = AtomicBoolean(true)
    private var disconnectionHandler = { _: Client -> }

    init {
        thread {
            try {
                while (connected.get()) {
                    val data = client.getInputStream().read()
                    if (data == -1) {
                        connected.set(false)
                        client.close()
                        disconnectionHandler(this)
                    } else {
                        received.add(data.toByte())
                    }
                }
            } catch (e: SocketException) {
            }
        }
    }

    fun readByteAlways(handler: (Byte) -> Unit) {
        while (true) {
            val data = received.poll()
            if (data != null) {
                handler(data)
            }
        }
    }

    fun readVarIntAlways(handler: (Int) -> Unit) {
        var numRead = 0
        var result = 0
        readByteAlways { data ->
            val value: Int = data.toInt() and 127
            result = result or (value shl 7 * numRead)
            numRead++

            if (numRead > 5) {
                throw RuntimeException("VarInt is too big")
            }

            if (data.toInt() and 128 == 0) {
                handler(result)
                result = 0
                numRead = 0
            }
        }

    }

    fun read(n: Int): Packet {
        val result = mutableListOf<Byte>()
        repeat(n) {
            while (true) {
                val data = received.poll()
                if (data != null) {
                    result.add(data)
                    break
                }
            }
        }

        return Packet(result)
    }

    fun send(packet: Packet) {
        val fullPacket = Packet().putVarInt(packet.bytes.size).bytes + packet.bytes
        client.getOutputStream().write(fullPacket.toByteArray())
    }

    fun close() {
        connected.set(false)
        disconnectionHandler(this)
        client.close()
    }

    fun onDisconnect(handler: (Client) -> Unit) {
        disconnectionHandler = handler
    }
}