package io.qubelet.minenet

import java.nio.ByteBuffer
import kotlin.experimental.and
import kotlin.experimental.or

open class Packet(val bytes: MutableList<Byte> = mutableListOf()) {
    fun putBytes(data: List<Byte>) {
        bytes.addAll(data)
    }

    fun putInt(data: Int): Packet {
        putBytes(ByteBuffer.allocate(4).putInt(data).array().toList())
        return this
    }

    fun putShort(data: Short): Packet {
        putBytes(ByteBuffer.allocate(2).putShort(data).array().toList())
        return this
    }

    fun putLong(data: Long): Packet {
        putBytes(ByteBuffer.allocate(8).putLong(data).array().toList())
        return this
    }

    fun putDouble(data: Double): Packet {
        putBytes(ByteBuffer.allocate(8).putDouble(data).array().toList())
        return this
    }

    fun putFloat(data: Float): Packet {
        putBytes(ByteBuffer.allocate(4).putFloat(data).array().toList())
        return this
    }

    fun putChar(data: Char): Packet {
        putBytes(ByteBuffer.allocate(2).putChar(data).array().toList())
        return this
    }

    fun putByte(data: Byte): Packet {
        putBytes(listOf(data))
        return this
    }

    fun putBoolean(data: Boolean): Packet {
        putBytes(listOf<Byte>(if (data) 0x01 else 0x00))
        return this
    }

    fun putString(data: String): Packet {
        putVarInt(data.length)
        putBytes(data.toByteArray().toList())
        return this
    }

    fun putVarInt(data: Int): Packet {
        var value = data
        do {
            var temp = (value and 127).toByte()
            // Note: >>> means that the sign bit is shifted with the rest of the number rather than being left alone
            value = value ushr 7
            if (value != 0) {
                temp = temp or 128.toByte()
            }
            putByte(temp)
        } while (value != 0)

        return this
    }

    fun getInt(): Int = ByteBuffer.wrap(bytes.getMany(4)).int

    fun getShort(): Short = ByteBuffer.wrap(bytes.getMany(2)).short

    fun getLong(): Long = ByteBuffer.wrap(bytes.getMany(8)).long

    fun getDouble(): Double = ByteBuffer.wrap(bytes.getMany(8)).double

    fun getFloat(): Float = ByteBuffer.wrap(bytes.getMany(4)).float

    fun getChar(): Char = ByteBuffer.wrap(bytes.getMany(2)).char

    fun getByte(): Byte = bytes.get()

    fun getBoolean(): Boolean = bytes.get().toInt() and 0x01 == 1

    fun getString(): String {
        val len = getVarInt()
        return bytes.getMany(len).toString()
    }

    fun getVarInt(): Int {
        var numRead = 0
        var result = 0
        var read: Byte
        do {
            read = getByte()
            val value = (read and 127).toInt()
            result = result or (value shl 7 * numRead)
            numRead++
            if (numRead > 5) {
                throw RuntimeException("VarInt is too big")
            }
        } while (read.toInt() and 128 != 0)

        return result
    }

    fun MutableList<Byte>.getMany(n: Int): ByteArray {
        val result = ByteArray(n)

        repeat(n) {
            result[it] = this[0]
            this.removeAt(0)
        }

        return result
    }

    fun MutableList<Byte>.get(): Byte {
        val data = this[0]
        this.removeAt(0)
        return data
    }
}