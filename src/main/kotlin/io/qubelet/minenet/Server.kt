package io.qubelet.minenet

import java.net.ServerSocket
import java.net.Socket
import kotlin.concurrent.thread

open class Server {
    lateinit var server: ServerSocket
    var onConnect = { _: Client -> }

    private fun handle(client: Client) {
        thread {
            onConnect(client)
        }
    }

    fun bind(port: Int) {
        server = ServerSocket(port)

        thread {
            while (true) {
                val client = server.accept()
                handle(Client(client))
            }
        }
    }

    fun onConnect(handler: (Client) -> Unit) { onConnect = handler }
}